/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.service;

import cr.ac.una.gestorcorreos.model.ProcesoDto;
import cr.ac.una.gestorcorreos.util.Request;
import cr.ac.una.gestorcorreos.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Usuario
 */
public class ProcesoService {

    public Respuesta getProceso(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ProcesoController/proceso", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            ProcesoDto procesoDto = (ProcesoDto) request.readEntity(ProcesoDto.class);
            return new Respuesta(true, "", "", "Proceso", procesoDto);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoService.class.getName()).log(Level.SEVERE, "Error obteniendo el proceso [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el proceso.", "getProceso " + ex.getMessage());
        }
    }
    
    public Respuesta getProcesos() {
        try {
            Request request = new Request("ProcesoController/procesos");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<ProcesoDto> empleados = (List<ProcesoDto>) request.readEntity(new GenericType<List<ProcesoDto>>() {
            });
            return new Respuesta(true, "", "", "Procesos", empleados);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoService.class.getName()).log(Level.SEVERE, "Error obteniendo procesos.", ex);
            return new Respuesta(false, "Error obteniendo procesos.", "getProcesos " + ex.getMessage());
        }
    }
    
    public Respuesta guardarProceso(ProcesoDto proceso) {
        try {
            
            Request request = new Request("ProcesoController/proceso");
            request.post(proceso);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            ProcesoDto procesoDto = (ProcesoDto) request.readEntity(ProcesoDto.class);
            return new Respuesta(true, "", "", "Proceso", procesoDto);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoService.class.getName()).log(Level.SEVERE, "Error guardando el proceso.", ex);
            return new Respuesta(false, "Error guardando el proceso.", "guardarProceso " + ex.getMessage());
        }
    }

    public Respuesta eliminarProceso(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ProcesoController/proceso", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoService.class.getName()).log(Level.SEVERE, "Error eliminando el proceso.", ex);
            return new Respuesta(false, "Error eliminando el proceso.", "eliminarProceso " + ex.getMessage());
        }
    }

}
