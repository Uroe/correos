/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.service;

import cr.ac.una.gestorcorreos.model.CorreoDto;
import cr.ac.una.gestorcorreos.util.Request;
import cr.ac.una.gestorcorreos.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Usuario
 */
public class CorreoService {

    public Respuesta getCorreo(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("CorreoController/correo", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            CorreoDto correoDto = (CorreoDto) request.readEntity(CorreoDto.class);
            return new Respuesta(true, "", "", "Correo", correoDto);
        } catch (Exception ex) {
            Logger.getLogger(CorreoService.class.getName()).log(Level.SEVERE, "Error obteniendo el correo [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el correo.", "getCorreo" + ex.getMessage());
        }
    }

    public Respuesta getCorreos() {
        try {
            Request request = new Request("CorreoController/correos");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<CorreoDto> correos = (List<CorreoDto>) request.readEntity(new GenericType<List<CorreoDto>>() {
            });
            return new Respuesta(true, "", "", "Correos", correos);
        } catch (Exception ex) {
            Logger.getLogger(CorreoService.class.getName()).log(Level.SEVERE, "Error obteniendo correos.", ex);
            return new Respuesta(false, "Error obteniendo correos.", "getCorreos " + ex.getMessage());
        }
    }

    public Respuesta guardarCorreo(CorreoDto correo) {
        try {
            Request request = new Request("CorreoController/correo");
            request.post(correo);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            CorreoDto correoDto = (CorreoDto) request.readEntity(CorreoDto.class);
            return new Respuesta(true, "", "", "Correo", correoDto);
        } catch (Exception ex) {
            Logger.getLogger(CorreoService.class.getName()).log(Level.SEVERE, "Error guardando el correo.", ex);
            return new Respuesta(false, "Error guardando el correo.", "guardarCorreo " + ex.getMessage());
        }
    }

    public Respuesta eliminarCorreo(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("CorreoController/correo", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(CorreoService.class.getName()).log(Level.SEVERE, "Error eliminando el correo.", ex);
            return new Respuesta(false, "Error eliminando el correo.", "eliminarCorreo" + ex.getMessage());
        }
    }

    public Respuesta enviarCorreo(String para, String asunto, String mensaje) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("para", para);
            parametros.put("asunto", asunto);
            parametros.put("mensaje", mensaje);
            Request request = new Request("CorreoController/correo", "/{para}/{asunto}/{mensaje}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(CorreoService.class.getName()).log(Level.SEVERE, "Error enviando el correo.", ex);
            return new Respuesta(false, "Error enviando el correo.", "enviarCorreo" + ex.getMessage());
        }
    }
}
