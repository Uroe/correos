package cr.ac.una.gestorcorreos;

import cr.ac.una.gestorcorreos.util.FlowController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        FlowController.getInstance().InitializeFlow(stage,null);
        stage.getIcons().add(new Image(App.class.getResourceAsStream("/cr/ac/una/gestorcorreos/resources/Usuario.png")));
        stage.setTitle("Gestor de Correos");
        FlowController.getInstance().goViewInWindow("LogInCorreo");
    }
    
    public static void main(String[] args) {
        launch();
    }

}