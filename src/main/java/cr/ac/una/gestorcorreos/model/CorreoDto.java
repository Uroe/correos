/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.model;

import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Ale
 */
public class CorreoDto {
    
    public SimpleStringProperty corId;
    public SimpleStringProperty corDestinatario;
    public SimpleStringProperty corAsunto;
    public SimpleStringProperty corContenido;
    public SimpleStringProperty corIdUsuario;
    public ObjectProperty<LocalDate> corFecha;
    public SimpleStringProperty corEstado;
    public ObjectProperty<LocalDate> fecha;
    public SimpleStringProperty nomUsuario;
    private Boolean modificado;

    public CorreoDto() {
        this.modificado = false;
        this.corId = new SimpleStringProperty();
        this.corDestinatario = new SimpleStringProperty();
        this.corAsunto = new SimpleStringProperty();
        this.corContenido = new SimpleStringProperty();
        this.corIdUsuario = new SimpleStringProperty();
        this.corFecha = new SimpleObjectProperty();
        this.corEstado = new SimpleStringProperty();
        this.nomUsuario = new SimpleStringProperty();
    }

    public String getNomUsuario() {
        return nomUsuario.get();
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario.set(nomUsuario);
    }

    public Long getCorId() {
        if(corId.get()!=null && !corId.get().isEmpty())
            return Long.valueOf(corId.get());
        else
            return null;
    }

    public void setCorId(Long corId) {
        this.corId.set(corId.toString());
    }

    public String getCorDestinatario() {
        return corDestinatario.get();
    }

    public void setCorDestinatario(String corDestinatario) {
        this.corDestinatario.set(corDestinatario.toString());
    }

    public String getCorAsunto() {
        return corAsunto.get();
    }

    public void setCorAsunto(String corAsunto) {
        this.corAsunto.set(corAsunto.toString());
    }

    public String getCorContenido() {
        return corContenido.get();
    }

    public void setCorContenido(String corContenido) {
        this.corContenido.set(corContenido.toString());
    }

    public Long getCorIdUsuario() {
        if(corIdUsuario.get()!=null && !corIdUsuario.get().isEmpty())
            return Long.valueOf(corIdUsuario.get());
        else
            return null;
    }

    public void setCorIdUsuario(Long corIdUsuario) {
        this.corIdUsuario.set(corIdUsuario.toString());
    }

    public LocalDate getCorFecha() {
        return corFecha.get();
    }

    public void setCorFecha(LocalDate corFecha) {
        this.corFecha.set(corFecha);
    }

    public String getCorEstado() {
        return corEstado.get();
    }

    public void setCorEstado(String corEstado) {
        this.corEstado.set(corEstado.toString());
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "CorreoDto{" + "corId=" + corId + ", corDestinatario=" + corDestinatario + ", corAsunto=" + corAsunto + ", corContenido=" + corContenido + ", corIdUsuario=" + corIdUsuario + ", corFecha=" + corFecha + ", corEstado=" + corEstado + ", fecha=" + fecha + ", modificado=" + modificado + '}';
    }
    
}
