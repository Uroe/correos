/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.model;


import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author JosueNG
 */
public class ProcesoDto {
    
    public SimpleStringProperty proId;
    public SimpleStringProperty proNombre;
    public SimpleStringProperty proMachote;
    
    private Boolean modificado;
    
    public ProcesoDto(){
         this.modificado = false;
         this.proId = new SimpleStringProperty();
         this.proNombre = new SimpleStringProperty();
         this.proMachote = new SimpleStringProperty();
    }

    public Long getProId() {
        if(proId.get()!=null && !proId.get().isEmpty())
            return Long.valueOf(proId.get());
        else
            return null;
    }

    public void setProId(Long proId) {
        this.proId.set(proId.toString());
    }

    public String getProNombre() {
        return proNombre.get();
    }

    public void setProNombre(String proNombre) {
        this.proNombre.set(proNombre);
    }

    public String getProMachote() {
        return proMachote.get();
    }

    public void setProMachote(String proMachote) {
        this.proMachote.set(proMachote);
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "ProcesoDto{" + "proId=" + proId + ", proNombre=" + proNombre + ", proMachote=" + proMachote + '}';
    }
    
}

