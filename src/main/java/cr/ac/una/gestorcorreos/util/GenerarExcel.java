/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerarExcel {

    File file = new File("");

    public GenerarExcel() {
    }

    public void crearExcel() {

        String fileName = "datos.xlsx";
        String filePath = file.getAbsolutePath() + fileName;
        //Seteando el nombre de la hoja donde agregaremos los items
        String hoja = "Hoja1";

        //Creando objeto libro de Excel
        XSSFWorkbook book = new XSSFWorkbook();
        XSSFSheet hoja1 = book.createSheet(hoja);

        //Cabecera de la hoja de excel
        String[] header = new String[]{"Correo electronico", "Nombre", "Apellidos"};

        //Aplicando estilo color negrita a los encabezados
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);//Seteando fuente negrita al encabezado del archivo excel
        style.setFont(font);

        XSSFRow row = hoja1.createRow(0);//se crea las filas
        for (int j = 0; j < header.length; j++) {
            XSSFCell cell = row.createCell(j);//Creando la celda de la cabecera en conjunto con la posición
            cell.setCellStyle(style); //Añadiendo estilo a la celda creada anteriormente
            cell.setCellValue(header[j]);//Añadiendo el contenido de nuestra lista de productos
        }

        File excelFile;
        excelFile = new File(filePath); // Referenciando a la ruta y el archivo Excel a crear
        try (FileOutputStream fileOuS = new FileOutputStream(excelFile)) {
            if (excelFile.exists()) { // Si el archivo existe lo eliminaremos
                excelFile.delete();
                System.out.println("Archivo eliminado.!");
            }
            book.write(fileOuS);
            fileOuS.flush();
            fileOuS.close();
            System.out.println("Archivo Creado.!");
            try {
                Runtime.getRuntime().exec("cmd /d start " + filePath);
                Runtime.getRuntime().exec("cmd /c start " + filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
