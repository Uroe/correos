/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.util;

/**
 *
 * @author Usuario
 */
public class MachoteExcel {

    String machote1;
    String usuNombre;
    String usuApellido;
    String correo;

    public MachoteExcel(String correo, String usuNombre, String usuApellido) {
        this.correo = correo;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.machote1 = machoteAgradecimiento();
    }

    public String getUsuApellido() {
        return usuApellido;
    }

    public void setUsuApellido(String usuApellido) {
        this.usuApellido = usuApellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getMachote1() {
        return machote1;
    }

    public void setMachote1(String machote1) {
        this.machote1 = machote1;
    }

    public String machoteAgradecimiento() {

        String machote = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "\n"
                + "<head>\n"
                + "    <title></title>\n"
                + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n"
                + "    <style type=\"text/css\">\n"
                + "        @media screen {\n"
                + "            @font-face {\n"
                + "                font-family: 'Lato';\n"
                + "                font-style: normal;\n"
                + "                font-weight: 400;\n"
                + "                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');\n"
                + "            }\n"
                + "\n"
                + "            @font-face {\n"
                + "                font-family: 'Lato';\n"
                + "                font-style: normal;\n"
                + "                font-weight: 700;\n"
                + "                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');\n"
                + "            }\n"
                + "\n"
                + "            @font-face {\n"
                + "                font-family: 'Lato';\n"
                + "                font-style: italic;\n"
                + "                font-weight: 400;\n"
                + "                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');\n"
                + "            }\n"
                + "\n"
                + "            @font-face {\n"
                + "                font-family: 'Lato';\n"
                + "                font-style: italic;\n"
                + "                font-weight: 700;\n"
                + "                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');\n"
                + "            }\n"
                + "        }\n"
                + "\n"
                + "        /* CLIENT-SPECIFIC STYLES */\n"
                + "        body,\n"
                + "        table,\n"
                + "        td,\n"
                + "        a {\n"
                + "            -webkit-text-size-adjust: 100%;\n"
                + "            -ms-text-size-adjust: 100%;\n"
                + "        }\n"
                + "\n"
                + "        table,\n"
                + "        td {\n"
                + "            mso-table-lspace: 0pt;\n"
                + "            mso-table-rspace: 0pt;\n"
                + "        }\n"
                + "\n"
                + "        img {\n"
                + "            -ms-interpolation-mode: bicubic;\n"
                + "        }\n"
                + "\n"
                + "        /* RESET STYLES */\n"
                + "        img {\n"
                + "            border: 0;\n"
                + "            height: auto;\n"
                + "            line-height: 100%;\n"
                + "            outline: none;\n"
                + "            text-decoration: none;\n"
                + "        }\n"
                + "\n"
                + "        table {\n"
                + "            border-collapse: collapse !important;\n"
                + "        }\n"
                + "\n"
                + "        body {\n"
                + "            height: 100% !important;\n"
                + "            margin: 0 !important;\n"
                + "            padding: 0 !important;\n"
                + "            width: 100% !important;\n"
                + "        }\n"
                + "\n"
                + "        /* iOS BLUE LINKS */\n"
                + "        a[x-apple-data-detectors] {\n"
                + "            color: inherit !important;\n"
                + "            text-decoration: none !important;\n"
                + "            font-size: inherit !important;\n"
                + "            font-family: inherit !important;\n"
                + "            font-weight: inherit !important;\n"
                + "            line-height: inherit !important;\n"
                + "        }\n"
                + "\n"
                + "        /* MOBILE STYLES */\n"
                + "        @media screen and (max-width:600px) {\n"
                + "            h1 {\n"
                + "                font-size: 32px !important;\n"
                + "                line-height: 32px !important;\n"
                + "            }\n"
                + "        }\n"
                + "\n"
                + "        /* ANDROID CENTER FIX */\n"
                + "        div[style*=\"margin: 16px 0;\"] {\n"
                + "            margin: 0 !important;\n"
                + "        }\n"
                + "    </style>\n"
                + "</head>\n"
                + "\n"
                + "<body style=\"background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;\">\n"
                + "    <!-- HIDDEN PREHEADER TEXT -->\n"
                + "    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\"> Proceso de cambio de contraseña </div>\n"
                + "    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "        <!-- LOGO -->\n"
                + "        <tr>\n"
                + "            <td bgcolor=\"#2AA9BD\" align=\"center\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\" valign=\"top\" style=\"padding: 40px 10px 40px 10px;\"> </td>\n"
                + "                    </tr>\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "        <tr>\n"
                + "            <td bgcolor=\"#2AA9BD\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#ffffff\" align=\"center\" valign=\"top\" style=\"padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;\">\n"
                + "                            <h1 style=\"font-size: 48px; font-weight: 400; margin: 2;\">Agradecimientos</h1>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "        <tr>\n"
                + "            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n"
                + "                            <p style=\"margin: 0;\">Estimado: " + usuNombre + "  " + usuApellido + "</p>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n"
                + "                            <p style=\"margin: 0;\">Por parte de la empresa queremos hacerte saber cuánto apreciamos tu ayuda con el proyecto.</p>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n"
                + "                            <p style=\"margin: 0;\">Sabemos cuánto tiempo y esfuerzo has invertido no solo para hacer el proyecto antes de la fecha límite, sino para asegurar que el cliente esté satisfecho con cada paso del proceso.</p>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n"
                + "                            <p style=\"margin: 0;\">¡Eres un miembro valioso de nuestro emplesa, y realmente aprecio tus contribuciones!.</p>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n"
                + "                            <p style=\"margin: 0;\">Saludos cordiales.<br>TheBigThree,<br>Equipo GS.</p>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\">\n"
                + "                            <img style=\"padding: 0; display: block;\" src=\"https://i.postimg.cc/9fp0wYtw/Agradecimiento.jpg\">\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "\n"
                + "        <tr>\n"
                + "            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 30px 10px 0px 10px;\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" width=\"20\">\n"
                + "                    <tr>\n"
                + "                        <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 30px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;\"> <br>\n"
                + "                            <p style=\"margin: 0;\">Email de notificacion.</p>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "    </table>\n"
                + "</body>\n"
                + "</html>";
        return machote;
    }

}
