/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

// Excel en Java, tutorial lectura
public class LeerExcel {

    File file = new File("");
    List<MachoteExcel> machotes = new ArrayList<>();

    public LeerExcel() {
    }

    public void leerExcel() {
        // Nombre del archivo a leer
        String fileName = "datos.xlsx";
        String pathFile = file.getAbsolutePath() + fileName;

        try (FileInputStream file = new FileInputStream(new File(pathFile))) {
            // Leer el archivo de Excel en la ruta especificada
            XSSFWorkbook book = new XSSFWorkbook(file);
            // Obtener medinte indice la hoja que se leera
            XSSFSheet sheet = book.getSheetAt(0);
            // Obteniendo todas las filas de la hoja leida
            Iterator<Row> rowIterator = sheet.iterator();

            // CRendo un StringBuilder donde almacenaremos los datos del 
            // Excel temporalmente para mostrarlo en consola
            StringBuilder sb = new StringBuilder();

            Row row;
            // Recorreremos cada fila de la hoja hasta llegar al final
            while (rowIterator.hasNext()) {
                row = rowIterator.next();
                // Obteniendo las celdas de cada fila para obtener valores
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell;
                //Recorrienco las celdas de la fila obtenida
                while (cellIterator.hasNext()) {
                    // Obteniendo el valor de la celda para ser almacenada en nuestro buffer
                    cell = cellIterator.next();
                    machotes.add(new MachoteExcel(cell.toString(), cellIterator.next().toString(), cellIterator.next().toString()));
                    // Agregando a nuestro buffer los valores leidos 
                    // de las celdas incluyendo la cabecera
                    // Haremos una pequeña validacion para saber si la columna que 
                    // estamos leyendo esta en la posicion cero, si es la primera no agregaremos formato
                    // de lo contrario rellenaremos con espacios la cadena pare verlo de forma ordenada.
                    if (cell.getColumnIndex() == 1) {
                        sb.append(cell.toString()).append("\t");
                    } else {
                        sb.append(String.format("%-30s", cell.toString())).append("\t");
                    }
                    AppContext.getInstance().set("Machotes", machotes);
                }
                // Agregando salto de linea, por estetica
                sb.append("\n");
                
            }
            // Imprimiendo contenido del Excel agregado en el buffer
            System.out.println(sb.toString());
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
