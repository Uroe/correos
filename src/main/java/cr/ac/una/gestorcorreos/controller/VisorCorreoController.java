/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.controller;

import cr.ac.una.gestorcorreos.util.AppContext;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author Usuario
 */
public class VisorCorreoController extends Controller implements Initializable {

    @FXML
    private WebView wbHtml;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        WebEngine webEngine = wbHtml.getEngine();
        webEngine.loadContent((String) AppContext.getInstance().get("proMachote"));
    }

    @Override
    public void initialize() {

    }

}
