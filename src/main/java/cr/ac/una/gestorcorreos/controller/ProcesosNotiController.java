/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorcorreos.model.ProcesoDto;
import cr.ac.una.gestorcorreos.service.ProcesoService;
import cr.ac.una.gestorcorreos.util.AppContext;
import cr.ac.una.gestorcorreos.util.FlowController;
import cr.ac.una.gestorcorreos.util.Formato;
import cr.ac.una.gestorcorreos.util.Mensaje;
import cr.ac.una.gestorcorreos.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class ProcesosNotiController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXCheckBox chkEstado;
    @FXML
    private JFXTextArea txtAMensajeCorreoHtml;
    @FXML
    private JFXButton btnVisCorreo;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnNuevo;

    ProcesoDto proceso;
    List<Node> requeridos = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtId.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(60));
        txtAMensajeCorreoHtml.setTextFormatter(Formato.getInstance().maxLengthFormat(20000));
        proceso = new ProcesoDto();
        nuevoProceso();
        indicarRequeridos();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindProceso(Boolean nuevo) {
        if (!nuevo) {
            txtId.textProperty().bind(proceso.proId);
        }
        txtNombre.textProperty().bindBidirectional(proceso.proNombre);
        txtAMensajeCorreoHtml.textProperty().bindBidirectional(proceso.proMachote);
    }

    private void unbindProceso() {
        txtId.textProperty().unbind();
        txtNombre.textProperty().unbindBidirectional(proceso.proNombre);
        txtAMensajeCorreoHtml.textProperty().unbindBidirectional(proceso.proMachote);
    }

    private void nuevoProceso() {
        unbindProceso();
        proceso = new ProcesoDto();
        bindProceso(true);
        txtId.clear();
        txtId.requestFocus();
        AppContext.getInstance().delete("proMachote");
    }

    private void cargarProceso(Long id) {
        ProcesoService service = new ProcesoService();
        Respuesta respuesta = service.getProceso(id);

        if (respuesta.getEstado()) {
            unbindProceso();
            proceso = (ProcesoDto) respuesta.getResultado("Proceso");
            bindProceso(false);
            validarRequeridos();
            AppContext.getInstance().set("proMachote", proceso.getProMachote());
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar proceso", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarProceso(Long.valueOf(txtId.getText()));
        }
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar proceso", getStage(), "¿Esta seguro que desea limpiar el proceso?")) {
            nuevoProceso();
        }
    }

    @FXML
    private void onActionBtnVisCorreo(ActionEvent event) {
        FlowController.getInstance().goViewInWindow("VisorCorreo");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar proceso", getStage(), invalidos);
            } else {
                ProcesoService service = new ProcesoService();
                Respuesta respuesta = service.guardarProceso(proceso);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar proceso", getStage(), respuesta.getMensaje());
                } else {
                    unbindProceso();
                    proceso = (ProcesoDto) respuesta.getResultado("Proceso");
                    bindProceso(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar proceso", getStage(), "Proceso actualizado correctamente.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ProcesosNotiController.class.getName()).log(Level.SEVERE, "Error guardando el proceso.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar proceso", getStage(), "Ocurrio un error guardando el proceso.");
        }
    }

    @Override
    public void initialize() {

    }
}
