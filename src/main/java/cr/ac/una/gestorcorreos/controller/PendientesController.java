/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.controller;

import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorcorreos.model.CorreoDto;
import cr.ac.una.gestorcorreos.service.CorreoService;
import cr.ac.una.gestorcorreos.util.AppContext;
import cr.ac.una.gestorcorreos.util.FlowController;
import cr.ac.una.gestorcorreos.util.Respuesta;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class PendientesController extends Controller implements Initializable {

    @FXML
    private TableView<CorreoDto> tbvCorPendientes;
    @FXML
    private TableColumn<CorreoDto, String> tbcUsuario;
    @FXML
    private TableColumn<CorreoDto, String> tbcCorreo;
    @FXML
    private TableColumn<CorreoDto, LocalDate> tbcFecha;
    @FXML
    private TableColumn<CorreoDto, Boolean> tbcVisualisador;
    @FXML
    private SplitMenuButton smbFiltros;
    @FXML
    private JFXTextField txtFiltro;

    CorreoService service = new CorreoService();
    List<CorreoDto> listaTabla = new ArrayList<>();
    List<CorreoDto> correosPendientes = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Respuesta r = service.getCorreos();

        listaTabla = (List<CorreoDto>) r.getResultado("Correos");

        correosPendientes = listaTabla.stream().filter(correo -> correo.getCorEstado().equals("P")).collect(Collectors.toList());

        tbvCorPendientes.getItems().addAll(correosPendientes);
        tbvCorPendientes.refresh();

        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        List<UsuarioDto> usuarioDtos = gestor.getUsuarios();
        List<Usuario> usuarios = new ArrayList<>();
        for (UsuarioDto usuarioDto : usuarioDtos) {
            usuarios.add(new Usuario(usuarioDto));
        }
        for (CorreoDto correodto : correosPendientes) {
            for (Usuario u : usuarios) {
                if (u.getUsuId().equals(correodto.getCorIdUsuario())) {
                    correodto.setNomUsuario(u.getUsuNombre());
                }
            }
        }

        tbcUsuario.setCellValueFactory(cd -> cd.getValue().nomUsuario);
        tbcCorreo.setCellValueFactory(cd -> cd.getValue().corDestinatario);
        tbcFecha.setCellValueFactory(cd -> cd.getValue().corFecha);

        tbcVisualisador.setCellValueFactory((TableColumn.CellDataFeatures<CorreoDto, Boolean> p) -> new SimpleBooleanProperty(p.getValue() != null));
        tbcVisualisador.setCellFactory((TableColumn<CorreoDto, Boolean> p) -> new PendientesController.ButtonCell());

        List<MenuItem> items = new ArrayList<>();
        items.add(new MenuItem("Remitente"));
        items.add(new MenuItem("Correo electronico"));
        items.add(new MenuItem("Fecha"));
        smbFiltros.getItems().addAll(items);
        smbFiltros.getItems().forEach((t) -> {
            t.setOnAction((h) -> {
                smbFiltros.setText(t.getText());
            });
        });
    }

    @FXML
    private void BuscarCorreos(KeyEvent event) {
        ObservableList<CorreoDto> gestiones = FXCollections.observableArrayList(correosPendientes);
        FilteredList<CorreoDto> filterData = new FilteredList<>(gestiones, p -> true);
        txtFiltro.textProperty().addListener((ov, t, t1) -> {
            filterData.setPredicate((cor) -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                String typedText = t1.toLowerCase();
                if (cor.nomUsuario.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Remitente")) {
                    return true;
                }
                if (cor.corDestinatario.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Correo electronico")) {
                    return true;
                }
                if (cor.corFecha.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Fecha")) {
                    return true;
                }
                return false;
            });
            SortedList<CorreoDto> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tbvCorPendientes.comparatorProperty());
            tbvCorPendientes.setItems(sortedList);
        });
    }

    private class ButtonCell extends TableCell<CorreoDto, Boolean> {

        final Button cellButton = new Button();

        ButtonCell() {
            cellButton.setPrefWidth(500);
            cellButton.getStyleClass().add("jfx-btnimg-tbvvisualisar");

            cellButton.setOnAction((ActionEvent t) -> {
                CorreoDto correo = (CorreoDto) ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                AppContext.getInstance().set("proMachote", correo.getCorContenido());
                FlowController.getInstance().goViewInWindow("VisorCorreo");
                FlowController.getInstance().initialize();
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    @Override
    public void initialize() {
    }

}
