/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorcorreos.util.AppContext;
import cr.ac.una.gestorcorreos.util.FlowController;
import cr.ac.una.gestorseguridad.ws.RolDto;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class PrincipalController extends Controller implements Initializable {

    @FXML
    private Label lblUsuario;
    @FXML
    private Label lblRol;
    @FXML
    private JFXButton btnRedactar;
    @FXML
    private JFXButton btnEnviados;
    @FXML
    private JFXButton btnPendientes;
    @FXML
    private JFXButton btnProcesoNoti;
    @FXML
    private JFXButton btnCerSession;
    @FXML
    private JFXButton btnSalir;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        UsuarioDto u = (UsuarioDto) AppContext.getInstance().get("Usuario");
        RolDto r = (RolDto) AppContext.getInstance().get("Rol");
        if (r.getRolNombre().equals("Normal")) {
            btnEnviados.setDisable(true);
            btnPendientes.setDisable(true);
            btnProcesoNoti.setDisable(true);
        }
        lblUsuario.setText(u.getUsuUsuario());
        lblRol.setText(r.getRolNombre());
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void onActionBtnRedactar(ActionEvent event) {
        FlowController.getInstance().goView("Correos");
    }

    @FXML
    private void onActionBtnEnvidos(ActionEvent event) {
        FlowController.getInstance().goView("Enviados");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionBtnPendientes(ActionEvent event) {
        FlowController.getInstance().goView("Pendientes");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionBtnProcesoNoti(ActionEvent event) {
        FlowController.getInstance().goView("ProcesosNoti");
    }

    @FXML
    private void onActionBtnCerSesion(ActionEvent event) {
        FlowController.getInstance().salir();
        FlowController.getInstance().goViewInWindow("LogInCorreo");
    }

    @FXML
    private void onActionBtnSalir(ActionEvent event) {
        FlowController.getInstance().salir();
    }

}
