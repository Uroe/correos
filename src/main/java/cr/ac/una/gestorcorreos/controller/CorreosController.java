/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcorreos.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorcorreos.model.CorreoDto;
import cr.ac.una.gestorcorreos.model.ProcesoDto;
import cr.ac.una.gestorcorreos.service.CorreoService;
import cr.ac.una.gestorcorreos.service.ProcesoService;
import cr.ac.una.gestorcorreos.util.AppContext;
import cr.ac.una.gestorcorreos.util.Correo;
import cr.ac.una.gestorcorreos.util.GenerarExcel;
import cr.ac.una.gestorcorreos.util.LeerExcel;
import cr.ac.una.gestorcorreos.util.MachoteExcel;
import cr.ac.una.gestorcorreos.util.Mensaje;
import cr.ac.una.gestorcorreos.util.Respuesta;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class CorreosController extends Controller implements Initializable {

    @FXML
    private SplitMenuButton smbProceso;
    @FXML
    private Label lblProceso;
    @FXML
    private JFXTextField txtAsunto;
    @FXML
    private JFXButton btnGenExcel;
    @FXML
    private JFXButton btnCargarExcel;
    @FXML
    private JFXButton btnGenCorreos;
    @FXML
    private JFXButton btnEnviarCorreos;

    CorreoDto correodto;
    UsuarioDto usuarioDto;
    ProcesoDto procesoDto;
    List<CorreoDto> correos = new ArrayList<>();
    List<CorreoDto> correosPendientes = new ArrayList<>();
    List<ProcesoDto> procesos = new ArrayList<>();
    List<ProcesoDto> procesosEx = new ArrayList<>();
    List<MenuItem> itemProceso = new ArrayList<>();
    List<MachoteExcel> machotes = new ArrayList<>();
    CorreoService service;
    Correo correo;
    List<File> archivo;
    File archi = null;
    @FXML
    private JFXButton btnAdjuntarArchivo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        correo = new Correo();
        correodto = new CorreoDto();
        service = new CorreoService();
        usuarioDto = (UsuarioDto) AppContext.getInstance().get("Usuario");
        procesoDto = new ProcesoDto();
        ProcesoService proceso = new ProcesoService();
        Respuesta resp = proceso.getProcesos();
        if (resp.getEstado()) {
            procesos = (List<ProcesoDto>) resp.getResultado("Procesos");
            for (ProcesoDto pro : procesos) {
                itemProceso.add(new MenuItem(pro.getProNombre()));
            }
            smbProceso.getItems().addAll(itemProceso);

            itemProceso.forEach(i -> {
                i.setOnAction((t) -> {
                    for (ProcesoDto pro : procesos) {
                        if (i.getText().equals(pro.getProNombre())) {
                            lblProceso.setText(pro.getProNombre());
                        }
                    }
                });
            });
        }
    }

    @Override
    public void initialize() {
        
    }

    private void actualizarCorreo(CorreoDto cor) {
        cor.setCorId(cor.getCorId());
        cor.setCorAsunto(cor.getCorAsunto());
        cor.setCorContenido(cor.getCorContenido());
        cor.setCorDestinatario(cor.getCorDestinatario());
        cor.setCorEstado("E");
        cor.setCorFecha(LocalDate.now());
        Respuesta resp = service.guardarCorreo(cor);
        correodto = (CorreoDto) resp.getResultado("Correo");
    }

    @FXML
    private void onActionBtnGenExcel(ActionEvent event) {
        GenerarExcel cre = new GenerarExcel();
        cre.crearExcel();
    }

    @FXML
    private void onActionBtnCargarExcel(ActionEvent event) {
        LeerExcel lee = new LeerExcel();
        lee.leerExcel();
    }

    @FXML
    private void onActionBtnAdjuntarArchivo(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        archivo = fileChooser.showOpenMultipleDialog(null);
        archi = archivo.get(0);
    }

    @FXML
    private void onActionBtnGenCorreos(ActionEvent event) {
        if (txtAsunto.getText().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Correo", getStage(), "El espacio de asunto esta vacio.");
        } else {
            Long id = usuarioDto.getUsuId();
            machotes = (List<MachoteExcel>) AppContext.getInstance().get("Machotes");
            for (int i = 1; i < machotes.size(); i++) {
                correodto.setCorAsunto(txtAsunto.getText());
                correodto.setCorContenido(machotes.get(i).getMachote1());
                correodto.setCorDestinatario(machotes.get(i).getCorreo());
                correodto.setCorEstado("P");
                correodto.setCorFecha(LocalDate.now());
                correodto.setCorIdUsuario(id);
                correodto.setCorIdUsuario(usuarioDto.getUsuId());
                CorreoService service = new CorreoService();
                Respuesta r = service.guardarCorreo(correodto);
            }
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "", getStage(), "Guardado con exito");
        }
    }

    @FXML
    private void onActionBtnEnviarCorreos(ActionEvent event) {
        CorreoService correoService = new CorreoService();
        Respuesta resp = correoService.getCorreos();
        if (resp.getEstado()) {
            correos = (List<CorreoDto>) resp.getResultado("Correos");
            correosPendientes = correos.stream().filter(c -> c.getCorEstado().equals("P")).collect(
                    Collectors.toList());
        }
        //Timer
        correosPendientes.forEach(c -> {
            ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
            ses.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    actualizarCorreo(c);
                    if (archi == null) {
                        correo.enviarCorreo(c.getCorDestinatario(), c.getCorAsunto(),
                                c.getCorContenido());
                    } else {
                        correo.enviarCorreoAdjun(c.getCorDestinatario(), c.getCorAsunto(),
                                c.getCorContenido(), archi);
                    }
                }
            }, 0, 1, TimeUnit.MINUTES);
        });

    }
}
