module cr.ac.una.gestorcorreos {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires java.ws.rs;
    requires java.xml.bind;
    requires java.sql;
    requires java.base;
    requires com.jfoenix;
    requires jakarta.activation;
    requires jakarta.mail;
    requires javafx.web;
    requires poi;
    requires poi.ooxml;
    requires poi.ooxml.schemas;
    requires cr.ac.una.gestorseguridad;

    opens cr.ac.una.gestorcorreos.controller to javafx.fxml, javafx.controls, com.jfoenix;
    opens cr.ac.una.gestorcorreos to jakarta.mail, jakarta.activation;
    exports cr.ac.una.gestorcorreos to javafx.graphics;
    exports cr.ac.una.gestorcorreos.model;

}
